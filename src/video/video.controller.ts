import { Controller, Get, Param, Res } from '@nestjs/common';
import { join } from 'path';
import { FfmpegService } from 'services/ffmpeg.service';
import { path } from '../settings/global.settings';


@Controller('video')
export class VideoController {
  // private readonly fileDirectory: string = 'C:/Users/p7586381/Desktop/camera-live';
  // private readonly fileDirectory: string = 'C:/Users/משתמש/Desktop/camera-live';

  private readonly fileDirectory: string = path;

  constructor(private FFmpegService: FfmpegService) {}

  @Get()
  mainpage() {
    console.log('User ask for supplying live');
    return 'Video Main Page';
  }

  @Get('/start-live/:videoName')
  startLive(@Param('videoName') videoName) {
    console.log('start live/:videoName');
    this.FFmpegService.startLive(videoName);
  }

  @Get('/stop-live')
  stopLive() {
    this.FFmpegService.stopLive();
  }

  @Get('/resume-live')
  resumeLive() {
    this.FFmpegService.resumeLive();
  }

  @Get('/kill-live')
  killLive() {
    this.FFmpegService.killLive();
  }

  @Get('/:deviceName/:segmentName')
  getSegment(@Param('segmentName') segmentName, @Param('deviceName') deviceName, @Res() res) {
    console.log("device name", deviceName, "segment", segmentName);
    let filePath: string = join(this.fileDirectory, deviceName, segmentName);
    console.log(filePath);

    // res.sendFile(filePath);
    res.sendfile(filePath);
  }
}
