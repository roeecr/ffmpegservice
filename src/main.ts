import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import { path } from 'settings/global.settings';

import { from } from 'rxjs';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  app.use(express.static(path));
  app.enableCors();

  console.log('listening ot http://localhost:7000');
  await app.listen(7000);
}
bootstrap();
